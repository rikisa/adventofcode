package main


import (
    "fmt"
    "sort"
    // "strconv"
    // "regexp"
    // "strings"
    // "errors"
    // "math"
    "aoc/inputs"
    "fatih/color"
)

var open []rune = []rune{'(', '[', '<', '{'}
var clos []rune = []rune{')', ']', '>', '}'}
func inArray(v rune, ar []rune) bool {
    for _, a := range(ar) {
        if a == v {
            return true
        }
    }
    return false
}

var scorepoints map[rune]int = map[rune]int{
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
}

var brackets map[rune]rune = map[rune]rune{
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',
}

var exscorepoints map[rune]int = map[rune]int{
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
}

func parse(ln int, s string) (int, string, bool) {
    if len(s) == 0 {
        return ln, "", true
    }
    nxt := rune(s[0])
    if inArray(nxt, open) {
        ln, sn, ok := parse(ln+1, s[1:])
        if sn == "" || !ok {
            return ln, sn, ok
        }
        db :=  rune(sn[0]) - nxt
        if db < 0 || db > 2 {
            return ln, sn, false
        }
        return parse(ln+1, sn[1:])
    }
    if inArray(nxt, clos) {
        return ln, s, true
    }
    // anything else
    return parse(ln+1, s[1:])
}

func parse2(s, e string) (string, string) {
    if len(s) == 0 {
        return s, e
    }
    nxt := rune(s[0])
    if inArray(nxt, open) {
        return parse2(s[1:], string(brackets[nxt]) + e)
    }
    if inArray(nxt, clos) {
        return parse2(s[1:], e[1:])
    }
    return parse2(s[1:], e[1:])
}

// Solution to part one
func solution() {
    rd := color.New(color.FgRed).SprintFunc()
    fmt.Printf("\n%s%s\n", ".", rd("."))
    // s := Stack{}
    // s.Push('(')string(brackets[nxt]) + 
    // s.Push('<')
    // s.Push(']')
    // s.Push('[')
    pts := 0
    for _, s := range(inputs.AsStrings()) {
        ln, r, ok := parse(0, s)
        if ok {
            fmt.Println(s)
        } else {
            fmt.Printf("%s%s%s | %s\n", s[:ln], rd(string(s[ln])), s[ln+1:], r)
            // fmt.Printf("%s %s(%d)\n", s, rd(r), ln)
	    pts += scorepoints[rune(r[0])]
        }
    }
    fmt.Printf("=> %d\n", pts)
}

func scoreEx(ex string) int {
    ret := 0
    for _, c := range(ex) {
        ret = ret * 5 + exscorepoints[rune(c)]
	// fmt.Println(ret, string(c))
    }
	// fmt.Println("---")
    return ret
}
    

// Solution to part two
func solution2() {
    ge := color.New(color.FgGreen).SprintFunc()
    rd := color.New(color.FgRed).SprintFunc()
    var exs []int
    for _, s := range(inputs.AsStrings()) {
        _, _, ok := parse(0, s)
        if ok {
            _, ex := parse2(s, "")
            _, _, ok := parse(0, s + ex)
            if ok {
                fmt.Println(s + ge(ex))
                exs = append(exs, scoreEx(ex))
            } else {
                fmt.Println(s + rd(ex))
            }
        } 
    }
    sort.Slice(exs, func(i int, j int) bool { return exs[i] > exs[j] })
    fmt.Printf("=> %d\n", exs[len(exs) / 2 + 1])
    fmt.Printf("%d\n", exs)
}


func main () {
    if inputs.Which() == 1 {
        solution()
    } else {
        solution2()
    }
    fmt.Printf("Done!\n")
}
