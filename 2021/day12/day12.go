package main


import (
    "fmt"
    "time"
    // "bytes"
    "regexp"
    "strings"
    "sync"
    // "math/rand"
    // "sort"
    // "strconv"
    // "errors"
    // "reflect"
    "aoc/inputs"
    // "fatih/color"
    // "rthornton128/goncurses"
)

type Node struct {
    Name string
    Conn []*Node
    Large bool
}

type Edge struct {
    from *Node
    to *Node
}

func (n *Node) Connect(o *Node) {
    for _, c := range n.Conn {
        if c.Name == o.Name {
            return
        }
    }
    n.Conn = append(n.Conn, o)
    o.Conn = append(o.Conn, n)
}

func (n Node) String() string {
    // var cs []string
    // for _, c := range n.Conn {
    //     cs = append(cs, c.Name)
    // }
    // c := strings.Join(cs, ", ")
    sgn := ""
    if n.Large {
        sgn = "!"
    }
    // return fmt.Sprintf("%s%s [%s]", sgn, n.Name, c)
    return fmt.Sprintf("%s%s", sgn, n.Name)
}

var smallNodes []*Node = []*Node{}
func Make(n string) Node {
    return Node{n, make([]*Node, 0), n[0] <= 90}
}

// return start node
func readGraph() *Node {

    var re = regexp.MustCompile(` *- *`)

    cache := map[string]*Node{}

    scanner := inputs.AsLineScannerFirst()
    for scanner.Scan() {
        ln := re.Split(scanner.Text(), 2)
        for _, nn := range ln {
            _, ok := cache[nn]
            if !ok {
                newNode := Make(nn)
                if !newNode.Large {
                    smallNodes = append(smallNodes, &newNode)
                }
                cache[nn] = &newNode
            }
        }
        cache[ln[0]].Connect(cache[ln[1]])
    }

    start, ok := cache["start"]
    if !ok {panic("No start!")}
    return start
}

func isVisited(n *Node, arr []*Node) bool {
    for _, dst := range arr {
        if n == dst {return true}
    }
    return false
}

func joinTr(tr []*Node) string  {
    names := []string{}
    for _, pi := range tr {
        names = append(names, pi.Name)
    }
    return strings.Join(names, ",")
}

// returns all the possible pathes
func walk(n *Node, trace []*Node, f *Node, depth int) [][]*Node {
    var pathes [][]*Node = [][]*Node{}

    // the end condition
    if n.Name == "end" {
        return [][]*Node{[]*Node{n}}
    }

    if depth >= 20 {
        ret := Make("Stopped")
        return [][]*Node{[]*Node{&ret}}
    }

    // welcome to the path
    trace = append(trace, n)
    // inDeadend := len(n.Conn) == 1
    for _, child := range n.Conn {

        // stop conditions
        if isVisited(child, trace) && !child.Large {
            // ret := Make("Trapped")
            // pathes = append(pathes, append([]*Node{n}, &ret))
            fmt.Printf("Trapped: %s(-%s)\n", joinTr(trace), child.Name)
            continue
        }

        // fmt.Printf("%s (<- %s) => %s\n", n.Name, f.Name, child.Name)
        // for each child start from current
        branches := walk(child, trace, n, depth+1)
        for _, cb := range branches {
            // add child path to newly created
            pathes = append(pathes, append([]*Node{n}, cb...))
        }
    }

    return pathes
}

func maxVisits(visitCount map[*Node]int, cmp int) int {
    ret := 0
    for n, v := range visitCount {
        if n.Name == "end" {continue}
        if v >= cmp {ret += 1}
    }
    return ret
}


// returns all the possible pathes
func walk2(n *Node, trace []*Node, visitCount map[*Node]int, f *Node, depth int) [][]*Node {
    var pathes [][]*Node = [][]*Node{}
    var wg sync.WaitGroup
    const revisitCap, revisitCount int = 2, 1

    if depth >= 200 {
        ret := Make("Stopped")
        return [][]*Node{[]*Node{&ret}}
    }

    // the end condition
    if n.Name == "end" {
        return [][]*Node{[]*Node{n}}
    }

    // fmt.Println(visitCount)
    c := make(chan [][]*Node, len(n.Conn))

    // welcome to the path
    trace = append(trace, n)
    // inDeadend := len(n.Conn) == 1
    for _, child := range n.Conn {
        // copy map
        chMap := map[*Node]int{}
        for k, v := range visitCount {
            chMap[k] = v
        }

        // // stop conditions
        // if isVisited(child, trace) && !child.Large {
        //     continue
        // }
        
        if !child.Large {
            switch {
                // ignore start
                case child.Name == "start":
                    continue
                // allowed maxVisits
                case chMap[child] == 0:
                    chMap[child] += 1
                    // fmt.Printf("Visit %d: %s>%s\n", chMap[child], joinTr(trace), child.Name)
                case chMap[child] == 1 && maxVisits(chMap, 2) == 0:
                    chMap[child] += 1
                    // fmt.Printf("Visit %d: %s>%s\n", chMap[child], joinTr(trace), child.Name)
                default:
                    // fmt.Printf("Trapped: %s(-%s)\n", joinTr(trace), child.Name)
                    continue
            }
        }

        // fmt.Printf("%s (<- %s) => %s\n", n.Name, f.Name, child.Name)
        // for each child start from current
        ch := child
        wg.Add(1)
        go func () {
            c <- walk2(ch, trace, chMap, n, depth+1)
            wg.Done()
        }()
    }
    
    wg.Wait()
    close(c)
    
    for branches := range c {
        for _, cb := range branches {
            // add child path to newly created
            pathes = append(pathes, append([]*Node{n}, cb...))
        }
    }

    return pathes
}

func (n *Node) Walk(rules int) (string, int) {
    var pathes [][]*Node
    if rules == 1 {
        pathes = walk(n, []*Node{}, n, 0)
    } else {
        pathes = walk2(n, []*Node{}, map[*Node]int{}, n, 0)}
    ret := ""
    for _, p := range pathes {
        ret += fmt.Sprintf("%s\n", joinTr(p))
    }
    return ret, len(pathes)
}


// Solution to part one
func solution(sel int) {
    t0 := time.Now()
    start := readGraph()
    traces, cnt := start.Walk(sel)
    dt := float32(time.Now().Sub(t0).Milliseconds())
    fmt.Printf("%s\n", traces)
    fmt.Printf("=> %d\n", cnt)
    fmt.Printf("Total: %.3f s\n", dt / 1000.0)
}

func main () {
    if inputs.WhichSecond() == 1 {
        solution(1)
    } else {
        solution(2)
    }
    fmt.Printf("Done!\n")
}
