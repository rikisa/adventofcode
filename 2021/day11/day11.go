package main


import (
    "fmt"
    "time"
    "bytes"
    "math/rand"
    // "sync"
    // "sort"
    // "strconv"
    // "regexp"
    // "strings"
    // "errors"
    // "reflect"
    "aoc/inputs"
    // "fatih/color"
    // "rthornton128/goncurses"
)

// func getLut() map[int][]byte{
//     rd := color.New(color.FgRed).SprintFunc()
//     bl := color.New(color.FgBlue).SprintFunc()
//     wi := color.New(color.FgWhite).SprintFunc()
//     yl := color.New(color.FgHiYellow).SprintFunc()
//     return map[int][]byte{
//         -1: []byte(bl(" ")),
//          1: []byte(wi(".")),
//          2: []byte(wi(".")),
//          3: []byte(wi(".")),
//          4: []byte(wi("+")),
//          5: []byte(wi("+")),
//          6: []byte(wi("+")),
//          7: []byte(wi("*")),
//          8: []byte(rd("*")),
//          9: []byte(rd("#")),
//          0: []byte(yl("#")),
//       -999: []byte(rd("?")),
//      }
// }

// // rgb
// func getLut() map[int][]byte{
//     return map[int][]byte{
//         -1: []byte(" "),
//          1: []byte("\033[38;2;150;120;0m*\033[0m"),
//          2: []byte("\033[38;2;100;50;0m*\033[0m"),
//          3: []byte("\033[38;2;50;0;0m+\033[0m"),
//          4: []byte("\033[38;2;25;0;0m+\033[0m"),
//          5: []byte("\033[38;2;80;0;0m+\033[0m"),
//          6: []byte("\033[38;2;110;25;0m*\033[0m"),
//          7: []byte("\033[38;2;140;50;0m*\033[0m"),
//          8: []byte("\033[38;2;175;100;0m*\033[0m"),
//          9: []byte("\033[38;2;200;180;0m*\033[0m"),
//          0: []byte("\033[38;2;255;230;100m*\033[0m"),
//       -999: []byte("?"),
//      }
// }

// rgb2

func getLut() map[int][]byte{
    return map[int][]byte{
        -1: []byte(" "),
         1: []byte("\033[38;2;150;120;0m▆\033[0m"),
         2: []byte("\033[38;2;100;50;0m▅\033[0m"),
         3: []byte("\033[38;2;50;0;0m▃\033[0m"),
         4: []byte("\033[38;2;25;0;0m▁\033[0m"),
         5: []byte("\033[38;2;80;0;0m▃\033[0m"),
         6: []byte("\033[38;2;110;25;0m▄\033[0m"), 
         7: []byte("\033[38;2;140;50;0m▅\033[0m"), 
         8: []byte("\033[38;2;175;100;0m▆\033[0m"),
         9: []byte("\033[38;2;200;180;0m▆\033[0m"),
         0: []byte("\033[38;2;255;230;100m▇\033[0m"),
      -999: []byte("?"),
     }
}

// func getLut() map[int][]byte{
//     return map[int][]byte{
//         -1: []byte(" "),
//          1: []byte("\033[38;5;16m*\033[0m"),
//          2: []byte("\033[38;5;17m*\033[0m"),
//          3: []byte("\033[38;5;52m*\033[0m"),
//          4: []byte("\033[38;5;52m*\033[0m"),
//          5: []byte("\033[38;5;88m*\033[0m"),
//          6: []byte("\033[38;5;88m*\033[0m"),
//          7: []byte("\033[38;5;125m*\033[0m"),
//          8: []byte("\033[38;5;160m*\033[0m"),
//          9: []byte("\033[38;5;160m*\033[0m"),
//          0: []byte("\033[38;5;15m#\033[0m"),
//       -999: []byte("?"),
//      }
// }

var byteMap map[int][]byte = getLut() 


// func render(c [][]int) string {
//     ret := ""
//     // rd := color.New(color.FgRed).SprintFunc()
//     for _, r := range(c) {
//         for _, v := range(r) {
//             switch {
//                 case v < 0: // not a squid
//                     ret += bl(-v)
//                 case v == 0: // flashing
//                     ret += yl(v)
//                 case v == 9 || v == 1: // will flash next
//                     // ret += rd(v)
//                     ret += fmt.Sprintf("%d", v)
//                 case v > 9: // will flash next
//                     // ret += rd(v)
//                     ret += "+"
//                 default:
//                     ret += fmt.Sprintf("%d", v)
//             }
//         }
//         ret += "\n"
//     }
//     return ret
// }

// func flash(power [][]int, ri, ci, flashAt int) {
//     var rii, cij, i, j int
// 
//     rdim := len(power) - 2
//     cdim := len(power[0]) - 2
// 
//     for i = -1; i <= 1; i++ {
//         rii = ri + i
//         if rii < 1 || rii > rdim {continue}
//         for j = -1; j <= 1; j++ {
//             cij = ci + j
//             // check if in bounds
//             if cij < 1 || cij > cdim {continue}
//             // // no self flashing
//             if rii == ri && cij == ci {continue}
// 
//             // increas power in neighbour to flash
//             if power[rii][cii] < flashAt {
//                 power[rii][cii] += 1
//             }
// 
//             // flash yerseld if needed
//             if power[rii][cii] == flashAt {
//                 flash(power, ri, ci, flashAt)
//             }
//         }
//     }
// }
// 
// func step(power [][]int, flashAt int) {
//     var ri, ci int
//     for ri = 1; ri < len(power) - 1; ri++ {
//         for ci = 1; ci < len(power[0]) - 1; ci++ {
//             _ := flash(power, ri, ci, flashAt)
//         }
//     }
// }

func makeSquids() Squids {
    sqarr := inputs.AsPaddedDenseDigitsOnly(-1)
    return Squids{
        arr: sqarr,
        rows: len(sqarr) - 1,
        cols: len(sqarr[0]) - 1, // needs to be square
        rowsp: len(sqarr),
        colsp: len(sqarr[0]), // needs to be square
        flashAt: 10,
        Count: (len(sqarr) - 2) * (len(sqarr[0]) - 2),
    }
}

type Squids struct {
    arr [][]int
    rows, cols, rowsp, colsp int
    stepdt time.Duration
    flashAt, Count int
}

// maps around padding
func (sq *Squids) At(r, c int) int {
    if r < sq.rows && c < sq.cols && r >= 0 && c >= 0 { 
        return sq.arr[r+1][c+1]
    }
    return -1
}

func (sq *Squids) Inc(r, c int) int {
    r, c = r + 1, c + 1
    if r < sq.rows && c < sq.cols && r > 0 && c > 0 { 
        sq.arr[r][c] += 1
        return sq.arr[r][c] 
    }
    return -1
}

func (sq *Squids) Null(r, c int) {
    if r < sq.rows && c < sq.cols && r >= 0 && c >= 0 { 
        sq.arr[r+1][c+1] = 0
    }
}

func (sq *Squids) flash(r, c int) int {
    var ri, cj, i, j, pij int
    var nf int = 1
    // will flash, above flashAt stops further rec
    sq.Inc(r, c)

    for i = -1; i <= 1; i++ {
        ri = r + i
        for j = -1; j <= 1; j++ {
            cj = c + j

            pij = sq.Inc(ri, cj)

            switch {
                case pij < 0:
                    continue
                case pij == sq.flashAt:
                    nf += sq.flash(ri, cj)
            }
        }
    }
    return nf
}

func (sq *Squids) Step() int {
    var ri, ci, nf int

    for ri = 0; ri < sq.rows; ri++ {
        for ci = 0; ci < sq.cols; ci++ {
            // random skip
            if rand.Float64() < 0.1 {continue}
            // inc and check for flashing
            if sq.Inc(ri, ci) == sq.flashAt {
                nf += sq.flash(ri, ci)
                // fmt.Printf("(%d, %d) flashed %d\n", ri, ci, nf)
            }
        }
    }
    // clip as part of flashing
    sq.clip()
    return nf
}

func (sq *Squids) clip() {
    var r, c int
    for r = 0; r < sq.rows; r++ {
        for c = 0; c < sq.cols; c++ {
            if sq.At(r, c) >= sq.flashAt {
                sq.Null(r, c)
            }
        }
    }
}

func (sq Squids) String() string {
    var buf bytes.Buffer
    rmax := sq.rowsp
    cmax := sq.colsp
    for r := 0; r < rmax; r++ {
    	for c := 0; c < cmax; c++ {
            bytes, ok := byteMap[sq.arr[r][c]]
            if ok {
    		    buf.Write(bytes)
            } else {
    		    buf.Write(byteMap[-999])
                // fmt.Printf("Invalid value lookup: %d @ (%d, %d)\n", sq.arr[r][c], r, c )
            }
    	}
    	buf.Write([]byte("\r\n"))
    }
    return buf.String()
}


// Solution to part one
func solution() {

    // src, err := goncurses.Init()
    // defer goncurses.End()
    // if err != nil {
    //     panic(err)
    // }
    // src.ClearOk(false)

    var start time.Time
    var ttot, tsim, tdraw time.Duration
    var ttotAvg, tsimAvg, tdrawAvg time.Duration
    var totalFlash, curFlash, maxFlash int
    waitMin, waitDraw := time.Millisecond * 100, time.Millisecond * 10
    totalSteps := 1000000
    
    time.Sleep(time.Millisecond * 500)
    fmt.Print("\033[2J") //Clear screen
    squids := makeSquids()
    fmt.Printf("%s", squids)

    m := -1

    for n := 1; n <= totalSteps; n++ {
        start = time.Now()

        curFlash = squids.Step()
        if curFlash > maxFlash {
            maxFlash = curFlash
        }
        totalFlash += curFlash

        if curFlash == squids.Count && m == -1 {
            m = n
        }
        tsim = time.Now().Sub(start)

        // fmt.Printf("\x0c%s", squids)
        fmt.Print("\033[2J") //Clear screen
        fmt.Printf("\033[%d;%dH", 0, 0) // Set cursor position
        fmt.Printf("Step: %3d/%3d\nFlashes: %3d/%3d(%3d)\nSync since: %3d\n%s", n, totalSteps, curFlash, squids.Count, maxFlash, m, squids.String())

        fmt.Printf("Wait: %4d/%4d/%4d µs/ms/ms (%7.1f/%3.1f/%3.1f UPS/FPS/FPS) ms\n",
                    tsim.Microseconds(),
                    tdraw.Milliseconds(),
                    ttot.Milliseconds(),
                    1000000 / float32(tsim.Microseconds()),
                    1000 / float32(tdraw.Milliseconds()),
                    1000 / float32(ttot.Milliseconds()),
                )
        nn := float32(n)
        fmt.Printf("Avg: %4.1f/%4.1f/%4.1f µs/ms/ms (%7.1f/%3.1f/%3.1f UPS/FPS/FPS) ms\n",
                    float32(tsimAvg.Microseconds()) / nn,
                    float32(tdrawAvg.Milliseconds()) / nn,
                    float32(ttotAvg.Milliseconds()) / nn,
                    nn * 1000000 / float32(tsimAvg.Microseconds()),
                    nn * 1000 / float32(tdrawAvg.Milliseconds()),
                    nn * 1000 / float32(ttotAvg.Milliseconds()),
                )
        time.Sleep(waitDraw)
        tdraw = time.Now().Sub(start)
        tsimAvg += tsim
        tdrawAvg += tdraw

        ttot = time.Now().Sub(start)
        ttotAvg += ttot
        if ttot < waitMin {
            time.Sleep(waitMin - ttot)
        }
        ttot = time.Now().Sub(start)
        
    }
    fmt.Print("\033[2J") //Clear screen
    fmt.Printf("\033[%d;%dH", 0, 0) // Set cursor position
    fmt.Printf("Step: %3d/%3d\nFlashes: %3d/%3d(%3d)\nSync since: %3d\n%s", totalSteps, totalSteps, curFlash, squids.Count, maxFlash, m, squids.String())
    fmt.Printf("\nWe got %d flashes!\n\n", totalFlash)
}

// Solution to part two
func solution2() {
}


func main () {
    solution()
    // if inputs.Which() == 1 {
    //     solution()
    // } else {
    //     solution2()
    // }
    fmt.Printf("Done!\n")
}
