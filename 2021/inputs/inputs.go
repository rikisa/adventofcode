package inputs


import (
    "flag"
    "math"
    "os"
    "bufio"
    "fmt"
    "strconv"
    "regexp"
    "strings"
)

// Simple error handler
func err_panic( err error ) {
    if err != nil{
        panic(err)
    }
}


// Read data from file  provided as command line argument
func getScanner () *bufio.Scanner {
    // get data input from commandline
    flag.Parse()
    fname := flag.Arg(1)
    fmt.Printf("Reading input from '%s'\n", fname)

    // read file contents
	file, err := os.Open(fname)
    err_panic(err)
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

    return scanner
}

// Read data from file  provided as command line argument
func getScannerOnly () *bufio.Scanner {
    // get data input from commandline
    flag.Parse()
    fname := flag.Arg(0)
    fmt.Printf("Reading input from '%s'\n", fname)

    // read file contents
	file, err := os.Open(fname)
    err_panic(err)
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

    return scanner
}

func AsLineScanner()  *bufio.Scanner {
    return getScanner()
}


func AsLineScannerFirst()  *bufio.Scanner {
    return getScannerOnly()
}

// Interprets data in file as strings
func AsStrings () []string {

    scanner := getScanner()
	var lines []string

    for scanner.Scan() {
		lines = append(lines, string(scanner.Text()))
	}
    
    return lines
}

// Interprets data in file as ints
func AsInts () []int {

    scanner := getScanner()
	var numbers []int

    for scanner.Scan() {
        num, err := strconv.Atoi(scanner.Text())
        err_panic(err)
		numbers = append(numbers, num)
	}
    
    return numbers
}

type KeyVal struct {
    Key string
    Val int
}

// Interpret data as string key: int value pair
func AsParams () []KeyVal {
    // var re = regexp.MustCompile(`^.* [0-9]+$`)

    // scanner := getScanner()
    // for scanner.Scan() {
    //     matches := re.FindAllString(scanner.Text(), -1)
    //     fmt.Printf("Word: %s Number: %s\n", matches[0], matches[1])
    // }

    var retvals []KeyVal

    var re = regexp.MustCompile(` +`)

    scanner := getScanner()
    for scanner.Scan() {
        matches := re.Split(scanner.Text(), -1)
        value, err := strconv.Atoi(matches[1])
        err_panic(err)
        retvals = append(retvals, KeyVal{matches[0], value}) 
    }

    return retvals
}

type Point struct {
    X int
    Y int
}

// Interpret data as x, y int points
func AsPoints () []*Point {

    var retvals []*Point

    var re = regexp.MustCompile(` *, *`)

    scanner := getScannerOnly()
    for scanner.Scan() {
        pt := [2]int{}
        ln := scanner.Text()
        if ln == "" {break}
        for i, v := range re.Split(ln, 2) {
            asint, err := strconv.Atoi(v)
            err_panic(err)
            pt[i] = asint
        }
        newPt := Point{pt[0], pt[1]}
        retvals = append(retvals, &newPt)
    }

    return retvals
}

// Interpret data as x, y int points
func AsFoldPoints() ([]*Point, []*KeyVal) {

    var retvals []*Point
    var retFolds []*KeyVal

    var re = regexp.MustCompile(` *, *`)
    var fre = regexp.MustCompile(` *= *`)

    scanner := getScannerOnly()
    for scanner.Scan() {
        pt := [2]int{}
        ln := scanner.Text()
        if ln == "" {break}
        for i, v := range re.Split(ln, 2) {
            asint, err := strconv.Atoi(v)
            err_panic(err)
            pt[i] = asint
        }
        newPt := Point{pt[0], pt[1]}
        retvals = append(retvals, &newPt)
    }

    for scanner.Scan() {
        ln := scanner.Text()
        fold := fre.Split(ln, 2)
        ax := string(fold[0][len(fold[0]) - 1])
        asint, err := strconv.Atoi(fold[1])
        err_panic(err)
        newFold := KeyVal{ax, asint}
        retFolds = append(retFolds, &newFold)
    }

    return retvals, retFolds
}

func Which () int {

    flag.Parse()
    pn, err := strconv.Atoi(flag.Arg(0))
    err_panic(err)
    fmt.Printf("Problem %d\n", pn)
    return pn
}

func WhichSecond () int {

    flag.Parse()
    pn, err := strconv.Atoi(flag.Arg(1))
    if err != nil {
        pn = 1
    }
    fmt.Printf("Problem %d\n", pn)
    return pn
}

func ToDec ( bits string ) int {
    var ret float64
    var bitdepth float64 = float64(len(bits))
    for i, bitval := range(bits) {
        if bitval == '1' {
            ret += math.Pow(2, bitdepth - float64(i) - 1)
        }
    }
    return int(ret)
}


type BingoNumber struct {
    Value, Row, Col int
    Checked bool
}

type Bingo struct {
    Numbers []BingoNumber
    Wins bool
    NumbersSeen int
    RowCount, ColCount []int
}

// uses first line only
func AsCSV () []int {
    var retvals []int

    var re = regexp.MustCompile(` *, *`)

    scanner := getScanner()
    scanner.Scan()
    for _, stringval := range(re.Split(scanner.Text(), -1)) {
        value, err := strconv.Atoi(stringval)
        err_panic(err)
        retvals = append(retvals, value) 
    }

    return retvals
}

func AsBingos () ([]int, []Bingo) {
    var retvals []int
    var retBingos []Bingo

    var re = regexp.MustCompile(` *, *`)

    scanner := getScanner()
    scanner.Scan()
    for _, stringval := range(re.Split(scanner.Text(), -1)) {
        value, err := strconv.Atoi(stringval)
        err_panic(err)
        retvals = append(retvals, value) 
    }
    
    var curbingo *Bingo
    for scanner.Scan() {
        rowString := scanner.Text()
        // fmt.Printf("Processing line '%s'\n", rowString)
        values := strings.Fields(rowString)
        if len(values) == 0 {
            // no more values, end bingo if needed
            if curbingo != nil {
                curbingo = nil

                // fmt.Println("Closed Bingo:")
                // for _, num := range(retBingos[len(retBingos) - 1].Numbers) {
                //     fmt.Println(num)
                // }
                // fmt.Println(retBingos[len(retBingos) - 1])
            }
            continue
        } else if len(values) > 0 {
            // values if bingo does not exists, create one
            if curbingo == nil {
                retBingos = append(retBingos, Bingo{})
                curbingo = &retBingos[len(retBingos) - 1]
                // fmt.Println("New Bingo created")
            }
            var newRow []int
            for _, colstring := range(values) {
                colint, err := strconv.Atoi(colstring)
                err_panic(err)
                newRow = append(newRow, colint) 
            }
            curbingo.AddRow(newRow)
            // curbingo.Values = append(curbingo.Values, newRow)
            // fmt.Printf("Append Values %d\n", newRow)
        }
    }

    return retvals, retBingos
}

func (b Bingo) String() string {
    // header and col counts
    var ret string
    var sep string
    for _, ccnt := range(b.ColCount) {
        ret += fmt.Sprintf("%3d ", ccnt)
        sep += fmt.Sprintf("%3s ", "^")
    }
    ret += "\n" + sep + "\n"

    // buffer array
    var numberArray [][]int = make([][]int, len(b.RowCount))
    for i := range(numberArray) {
        numberArray[i] = make([]int, len(b.ColCount))
    }
    
    // fill with values
    for _, num := range(b.Numbers) {
        numberArray[num.Row][num.Col] = num.Value
    }
    
    // add array to return string
    for ri, row := range(numberArray) {
        ret = ret + fmt.Sprintf("%3d", row)
        ret += fmt.Sprintf(" > %d\n", b.RowCount[ri])
    }
    return ret
}

func (b *Bingo) AddRow(newRow []int) {
    rowNum := len(b.RowCount)
    for colNum, val := range(newRow) {
        b.Numbers = append(b.Numbers, BingoNumber{Value:val, Row:rowNum, Col:colNum})
    }
    b.RowCount = append(b.RowCount, 0)
    
    switch {
        case b.ColCount == nil:
            b.ColCount = make([]int, len(newRow))
        case len(newRow) != len(b.ColCount):
            panic("Rowlen changed!")
    }
}

func (n *BingoNumber) Check() {
    n.Checked = true
}

func (b *Bingo) CheckNumber(number int) {
    for i, num := range(b.Numbers) {
        if (num.Checked == false) && (num.Value == number) {
            b.Numbers[i].Checked = true
            b.RowCount[num.Row] += 1
            b.ColCount[num.Col] += 1
        }
    }
    for _, rcnt := range(b.RowCount) {
        if rcnt == len(b.ColCount) {
            b.Wins = true
        }
    }

    for _, ccnt := range(b.ColCount) {
        if ccnt == len(b.RowCount) {
            b.Wins = true
        }
    }
}

// interpret lines as densly packed single digits
func AsDenseDigits() [][]int {

    var row []int
    var digits [][]int

    scanner := getScanner()

    for scanner.Scan() {
        row = []int{}
        for _, r := range(scanner.Text()) {
            // v, e := strconv.Atoi(r)
            // if e != nil {panic(e)}
            // row = append(row, v)
            row = append(row, int(r - '0'))
        } 
        digits = append(digits, row)
    }
    return digits
}

// same as DenseDigtis but with padding values
func AsPaddedDenseDigits(padVal int) [][]int {
    
    digits := AsDenseDigits()
    
    // cols
    for i, r := range(digits) {
        digits[i] = append(append([]int{padVal}, r...), padVal)
    }

    // rows
    padRow := make([]int, len(digits[0]))
    for i := range(padRow) {
        padRow[i] = padVal
    }
    digits = append([][]int{padRow}, append(digits, padRow)...)
    
    return digits
}

// interpret lines as densly packed single digits
func AsDenseDigitsOnly() [][]int {

    var row []int
    var digits [][]int

    scanner := getScannerOnly()

    for scanner.Scan() {
        row = []int{}
        for _, r := range(scanner.Text()) {
            // v, e := strconv.Atoi(r)
            // if e != nil {panic(e)}
            // row = append(row, v)
            row = append(row, int(r - '0'))
        } 
        digits = append(digits, row)
    }
    return digits
}

func PadArr(digits [][]int, padVal int) [][]int {
    // cols
    for i, r := range(digits) {
        digits[i] = append(append([]int{padVal}, r...), padVal)
    }

    // rows
    padRow := make([]int, len(digits[0]))
    for i := range(padRow) {
        padRow[i] = padVal
    }
    return append([][]int{padRow}, append(digits, padRow)...)
}

// same as DenseDigtis but with padding values
func AsPaddedDenseDigitsOnly(padVal int) [][]int {
    
    digits := AsDenseDigitsOnly()
    
    
    return PadArr(digits, padVal)
}
