package main


import (
    "fmt"
    // "time"
    // "bytes"
    // "regexp"
    "strings"
    // "sync"
    // "math/rand"
    // "sort"
    // "strconv"
    // "errors"
    // "reflect"
    i "aoc/inputs"
    // "fatih/color"
    // "rthornton128/goncurses"
)

func draw(arr []*i.Point) string {
    maxX, maxY := 0, 0
    for _, pt := range arr {
        if pt.Y > maxY {maxY = pt.Y}
        if pt.X > maxX {maxX = pt.X}
    }
    count := 0
    canvas := make([][]string, maxY + 1)
    for i := range canvas {
        canvas[i] = make([]string, maxX + 1)
        for j := range canvas[i] {
            canvas[i][j] = "."
        }
    }
    for _, pt := range arr {
        if canvas[pt.Y][pt.X] == "." {
            count += 1
        }
        canvas[pt.Y][pt.X] = "#"
    }
    
    ret := ""
    for _, row := range canvas {
        ret += fmt.Sprintf("%s\n", strings.Join(row, ""))
    }

    return fmt.Sprintf("%s=> %d", ret, count)
}

func foldx(pts []*i.Point, xax int) {
    for _, pt := range pts {
        if pt.X > xax {
            pt.X = (2*xax) - pt.X
        }
    }
}

// not removing doubling points
func foldy(pts []*i.Point, yax int) {
    for _, pt := range pts {
        if pt.Y > yax {
            pt.Y = (2*yax) - pt.Y
        }
    }
}


// Solution to part one
func solution(sel int) {
    // foldF := map[string]func([]*i.Point, int){
    //     "x": foldx,
    //     "y": foldy,
    // }

    pts, folds := i.AsFoldPoints()
    fmt.Println(draw(pts))
    // for _, p := range folds { 
    //     fmt.Println("")
    //     foldF[p.Key](pts, p.Val)
    //     draw(pts)
    // }
    for _, p := range folds { 
        switch{
            case p.Key == "y":
                foldy(pts, p.Val)
            case p.Key == "x":
                foldx(pts, p.Val)
        }
        fmt.Printf("%s=%d\n%s\n", p.Key, p.Val, draw(pts))
        if sel == 1 {break}
    }
}

func main () {
    if i.WhichSecond() == 1 {
        solution(1)
    } else {
        solution(2)
    }
    fmt.Printf("Done!\n")
}
