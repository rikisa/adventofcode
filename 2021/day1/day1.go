package main


import (
    "fmt"
    "aoc/inputs"
)

// Returns 1 if 'next' increased ('prev' < 'next') else 0
func is_increased ( prev, next int) int {
    if prev < next {
        return 1
    }
    return 0
}

func sum ( elems []int ) int {
    var ret int = 0
    for i := 0; i < len(elems); i++ {
        ret += elems[i]
    }
    return ret
}


// Solution to first and second day1 problem as described above
func problem1_2 ( winsize int ) {
    
    dat := inputs.AsInts()

    var increased int = 0
    last := sum(dat[0:winsize])
    fmt.Printf("%d (NA)\n", last)
    for i := 1; i <= (len(dat) - winsize); i++ {
        current := sum(dat[i:i+winsize])
        // if current > last {
        //     increased += 1
        //     fmt.Printf("%d (increased)\n", current)
        // } else {
        //     fmt.Printf("%d (decreased)\n", current)
        // }
        
        switch {
            case current > last:
                increased += 1
                fmt.Printf("%d (increased)\n", current)
            case current < last:
                fmt.Printf("%d (decreased)\n", current)
            case current == last:
                fmt.Printf("%d (not changed)\n", current)
        }
        last = current
    }

    fmt.Printf("Counted '%d' increased\n", increased)
}


func main () {
    if inputs.Which() == 1 {
        problem1_2(1)
    } else {
        problem1_2(3)
    }
}
