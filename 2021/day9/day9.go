package main


import (
    "fmt"
    "sort"
    // "strconv"
    // "regexp"
    // "strings"
    // "errors"
    // "math"
    "aoc/inputs"
    "fatih/color"
)

// func getNeighboursHV(r, c int, arr [][]int) []int {
//     // return []int{arr[r-1][c], arr[r+1][c], arr[r][], arr[][]}
//     var ret []int
//     for i := -1; i <= 1; i += 2 {
//         ret = append(ret, []int{arr[r+i][c], arr[r][c+i]}...)
//     }
//     return ret
// }

type Point struct {
    Row, Col, Val int
}

func (pt Point) String() string {
    return fmt.Sprintf("(%d,%d)=%d", pt.Row, pt.Col, pt.Val)
}

func getNeighboursHVPnt(cnt Point, th int, arr [][]int) []Point {
    var ret []Point
    var cand1, cand2 Point
    for i := -1; i <= 1; i += 2 {
        cand1 = Point{cnt.Row+i, cnt.Col, arr[cnt.Row+i][cnt.Col]}
        cand2 = Point{cnt.Row, cnt.Col+i, arr[cnt.Row][cnt.Col+i]}
        if cand1.Val < th {
            ret = append(ret, cand1)
        }
        if cand2.Val < th {
            ret = append(ret, cand2)
        }
    }
    return ret
}


// Solution to part one
func solution() {
    
    yl := color.New(color.FgHiYellow).SprintFunc()
    gr := color.New(color.FgWhite).SprintFunc()
    
    pMap := inputs.AsPaddedDenseDigits(10)
    riskLvl := 0
    var sidx string
    // var minima []Point
    for ri := 1; ri < len(pMap) - 1; ri++ {
        for ci := 1; ci < len(pMap[0]) - 1; ci++ {
            low := 0
            curPt := Point{ri, ci, pMap[ri][ci]}
            curNeighb := getNeighboursHVPnt(curPt, 11, pMap)
            // fmt.Printf("%s -> %s\n", curPt, curNeighb)
            // includ padding to make low cond easier
            for _, nv := range(curNeighb) {
                if nv.Val > curPt.Val {
                    // minima = append(minima, curPt)
                    low += 1
                }
            }
            // fmt.Printf("(%d, %d) %d %d\n", ri, ci, cval, neigh)
            if low == 4 {
                riskLvl += curPt.Val + 1
                sidx = yl(curPt.Val)
            } else {
                sidx = gr(curPt.Val)
            }
            fmt.Printf("%3s", sidx) 
            // fmt.Printf("%v(%d) ", curPt, low) 
        }
        fmt.Printf("\n")
    }
    fmt.Printf("=> %d\n", riskLvl)
}

func alreadyIn(pt Point, ptArr []Point) bool {
    for _, knpt := range(ptArr) {
        if knpt == pt {
            return true
        }
    }
    return false
}

func floodBasin(basin []Point, harr [][]int) []Point {
    ret := make([]Point, len(basin))
    copy(ret, basin)
    for _, curPt := range(basin) {
        npts := getNeighboursHVPnt(curPt, 9, harr) 
        for _, npt := range(npts) {
            if !alreadyIn(npt, ret) {
                ret = append(ret, npt)
            }
        }
    }
    if len(ret) == len(basin) {
        return ret
    } else {
        return floodBasin(ret, harr)
    }
}

// Solution to part two
func solution2() {

    yl := color.New(color.FgHiYellow).SprintFunc()
    gr := color.New(color.FgWhite).SprintFunc()

    // get minimas
    pMap := inputs.AsPaddedDenseDigits(10)
    var minima []Point
    for ri := 1; ri < len(pMap) - 1; ri++ {
        for ci := 1; ci < len(pMap[0]) - 1; ci++ {
            low := 0
            curPt := Point{ri, ci, pMap[ri][ci]}
            curNeighb := getNeighboursHVPnt(curPt, 11, pMap)
            for _, nv := range(curNeighb) {
                if nv.Val > curPt.Val {
                    low += 1
                }
            }
            if low == 4 {
                minima = append(minima, curPt)
            }
        }
    }
    // fmt.Printf("%s\n", minima)
    // yl := color.New(color.FgHiYellow).SprintFunc()
    // gr := color.New(color.FgWhite).SprintFunc()
    // minima = minima[:1]
    // basins := make(chan [][]Point, len(minima))
    basins := make([][]Point, len(minima))
    for bi, mi := range(minima) {
        basins[bi] = append(basins[bi], floodBasin([]Point{mi}, pMap)...)
    }
    for ri := 1; ri < len(pMap) - 1; ri++ {
        for ci := 1; ci < len(pMap[0]) - 1; ci++ {
            pt := Point{ri, ci, pMap[ri][ci]}
            inBasin := false
            for _, bs := range(basins) {
                if alreadyIn(pt, bs) {
                    inBasin = true
                    break
                }
            }
            if inBasin {
                fmt.Printf("%s", yl(pt.Val))
            } else {
                fmt.Printf("%s", gr(pt.Val))
            }
        }
        fmt.Printf("\n")
    }
    basinSizes := make([]int, len(basins))
    for bi, bs := range(basins) {
        // fmt.Printf("%v (%d)\n", bs, len(bs))
        basinSizes[bi] = len(bs)
    }

    sort.Slice(basinSizes, func(i int, j int) bool { return basinSizes[i] > basinSizes[j] })

    fmt.Printf("%d", basinSizes)
    bprod := 1
    for i := 0; i<3; i++ {
        bprod *= basinSizes[i]
    }
    fmt.Printf(" %d\n", bprod)
}


func main () {
    if inputs.Which() == 1 {
        solution()
    } else {
        solution2()
    }
    fmt.Printf("Done!\n")
}
