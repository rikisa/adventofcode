package main


import (
    "fmt"
    // "errors"
    "math"
    "sync"
    // "strconv"
    // "regexp"
    // "strings"
    "aoc/inputs"
)

type TestCase struct {
    Hor, Fuel int
}

func findBest(low, hi int, initHor []int) TestCase {
    var best, fuel int
    var diff float64
    for _, curPos := range(initHor) {
        diff = math.Abs(float64(low - curPos))
        best += int(diff)
    }
    var ret = TestCase{low, best}

    for curTest := low+1; curTest < hi; curTest++ {
        fuel = 0
        for _, curPos := range(initHor) {
            diff = math.Abs(float64(curTest - curPos))
            fuel += int(diff)
        }
        if fuel < best {
            best = fuel
            ret = TestCase{curTest, fuel}
        }
    }
    return ret
}

func findBest2(low, hi int, initHor []int) TestCase {
    var best, fuel int
    var diff float64
    for _, curPos := range(initHor) {
        diff = math.Abs(float64(low - curPos))
        diff = (diff * diff + diff) * 0.5
        best += int(diff)
    }
    var ret = TestCase{low, best}

    for curTest := low+1; curTest < hi; curTest++ {
        fuel = 0
        for _, curPos := range(initHor) {
            diff = math.Abs(float64(curTest - curPos))
            diff = (diff * diff + diff) * 0.5
            fuel += int(diff)
        }
        if fuel < best {
            best = fuel
            ret = TestCase{curTest, fuel}
        }
    }
    return ret
}

// Solution to part one
func solution(worker func(int, int, []int)TestCase) {
    initHor := inputs.AsCSV()
    fmt.Printf("%d\n", initHor)
    
    var stepSize, maxStep int = 100, 1000

    solutions := make(chan TestCase, maxStep / stepSize)

    var wg sync.WaitGroup

    for i := 0; i < maxStep; i += stepSize {
        fmt.Printf("%d - %d\n", i, i+stepSize)
        lo := i
        hi := i + stepSize
        wg.Add(1)
        go func() {
            defer wg.Done()
            solutions <- worker(lo, hi, initHor)
        }()
    }

    wg.Wait()
    close(solutions)
    fmt.Printf("\n%d\n", len(solutions))
    
    var winner = TestCase{-1, -1}
    for res := range(solutions) {
        if winner.Hor == -1 {
            winner = res
        } else if winner.Fuel > res.Fuel {
            winner = res
        }
        fmt.Printf("%v\n", res)
    }
    fmt.Printf("=> %v\n", winner)
}


func main () {
    if inputs.Which() == 1 {
        solution(findBest)
    } else {
        solution(findBest2)
    }
    fmt.Printf("Done!\n")
}
