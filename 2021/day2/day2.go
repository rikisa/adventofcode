package main


import (
    "fmt"
    "errors"
    "aoc/inputs"
)

// Returns 1 if 'next' increased ('prev' < 'next') else 0
func is_increased ( prev, next int) int {
    if prev < next {
        return 1
    }
    return 0
}

func sum ( elems []int ) int {
    var ret int = 0
    for i := 0; i < len(elems); i++ {
        ret += elems[i]
    }
    return ret
}


// Solution to problems
func solution ( aim_factor int ) {
    route := inputs.AsParams()
    
    var hor, depth, aim int
    for _, kv := range(route) {
        // fmt.Printf("%s %d", kv.Key, kv.Val)
        switch {
            case kv.Key == "forward":
                hor += kv.Val
                depth += aim_factor * (aim  * kv.Val)
                // fmt.Printf(" adds %d to hor\n", kv.Val)
            case kv.Key == "down":
                depth += kv.Val * (1 - aim_factor)
                aim += kv.Val
                // fmt.Printf(" adds %d to depth\n", kv.Val)
            case kv.Key == "up":
                depth -= kv.Val * (1 - aim_factor)
                aim -= kv.Val
                // fmt.Printf(" adds %d to depth\n", kv.Val)
            default:
                panic(errors.New("Invalid command!"))
        }
    }
    fmt.Printf("Horizontal: %d | Depth: %d | Pos: %d\n", hor, depth, hor * depth)
}


func main () {
    if inputs.Which() == 1 {
        solution(0)
    } else {
        solution(1)
    }
    fmt.Printf("Done!\n")
}
