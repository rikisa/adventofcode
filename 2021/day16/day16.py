import sys

def ba2str(bitarr):
    return ''.join(str(i) for i in bitarr)

class Transmission1:

    def __init__(self, bitarr):
        print(ba2str(bitarr))
        self.version = self.asdec(bitarr[:3])
        self.ttype = self.asdec(bitarr[3:6])
        self.bits = 6
        
        self.dat = -1
        self.subs = []
        if self.ttype == 4:
            self.desc = 'LIT'
            self.dat, datbits = self.readgroups(bitarr[6:])
            self.bits += datbits
            return

        if bitarr[6] == 1:
            # number of subs
            sub_count = self.asdec(bitarr[7:18])
            self.desc = f'OP{self.ttype}+{sub_count}x11b'
            for i in range(sub_count):
                i0 = (i * 11) + 18
                i1 = i0 + 11
                rest = bitarr[i0:]
                subp = Transmission1(rest)
                self.subs.append(subp)
        else:
            # 15 bits
            sub_bits = self.asdec(bitarr[7:22])
            rest = bitarr[22:]
            while len(rest) >= 11:
                subp = Transmission1(rest)
                self.subs.append(subp)
                rest = rest[subp.bits:]
            self.desc = f'OP{self.ttype}+{len(self.subs)}={sub_bits}b'

    @classmethod
    def from_hexstring(cls, hstring):
        as_bitarr = cls.asbitarr(int(hstring, base=16))

        # fill truncated values
        prepend = (len(hstring) * 4 - len(as_bitarr))
        if prepend > 0:
            as_bitarr = ([0] * prepend) + as_bitarr
        elif prepend < 0:
            print('AHHHHHH!')

        return cls(as_bitarr)

    def readgroups(self, bitarr):
        ret = []
        bitsRead = 0
        for i in range(0, len(bitarr), 5):
            ret += bitarr[i+1:i+5]
            bitsRead += 5
            if not bitarr[i]:
                break

        return self.asdec(ret), bitsRead

    def asdec(self, bitarr):
        ret = 0
        base = 1 << (len(bitarr) - 1)
        for val, bit in enumerate(bitarr):
            ret += bit * base
            base = base >> 1
        return ret
    
    @staticmethod
    def asbitarr(code):
        bitarr = []
        shiftbit = 1
        while shiftbit < code:
            bitarr.insert(0, int(code & shiftbit > 0))
            shiftbit = shiftbit << 1

        return bitarr

    def __str__(self):
        return self._str(0)

    def _str(self, d=0):
        ret = '  ' * d
        if self.ttype == 4:
            ret += f'<v{self.version}{self.desc}|{self.bits}b>{self.dat}'
        else:
            ret += f'<v{self.version}{self.desc}>'
            for sp in self.subs:
                ret += '\n' + sp._str(d + 1)
        return ret

def versum(tr):
    ret = tr.version
    for sp in tr.subs:
        ret += versum(sp)
    return ret

transmissions = []
with open(sys.argv[1], 'r') as src:
    ln = src.readline()
    while ln:
        transmissions.append(ln.strip())
        ln = src.readline()

for i, ctr in enumerate(transmissions[4:5]):
    print(ctr)
    tr = Transmission1.from_hexstring(ctr)
    print(tr)
    print(versum(tr))
# tr = transmissions[3]
# print(f'{tr}\n{Transmission1.from_hexstring(tr)}')
