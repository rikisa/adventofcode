import sys

def prod(args):
    ret = 1
    for arg in args:
        ret *= arg
    return ret


OPLUT = {
        0: ('+', lambda *args: sum(args)),
        1: ('*', lambda *args: prod(args)),
        2: ('m', lambda *args: min(args)),
        3: ('M', lambda *args: max(args)),
        5: ('>', lambda a, b: int(a > b)),
        6: ('<', lambda a, b: int(a < b)),
        7: ('=', lambda a, b: int(a == b)),
        }


class Package:

    def __init__(self, ver, ttype, dat):
        self.ver = ver
        self.ttype = ttype
        self.dat = dat
        self.subs = []

        if ttype == 4:
            self.desc = f'v{ver}LIT'
        else:
            opsymb, _ = OPLUT[ttype]
            self.desc = f'v{ver} OP{ttype}({opsymb})'

    def add_sub(self, pckg):
        self.subs.append(pckg)
        self.recalc()

    def recalc(self):
        _, func = OPLUT[self.ttype]
        if self.ttype > 4 and len(self.subs) < 2:
            self.dat = None
        else:
            self.dat = func(*[sp.dat for sp in self.subs])

    def _str(self):
        return f'{self.desc}: {self.dat}'

def ba2str(bitarr):
    return ''.join(str(i) for i in bitarr)

def from_hexstring(hstring):
    as_bitarr = asbitarr(int(hstring, base=16))

    # fill truncated values
    prepend = (len(hstring) * 4 - len(as_bitarr))
    if prepend > 0:
        as_bitarr = ([0] * prepend) + as_bitarr
    elif prepend < 0:
        print('AHHHHHH!')

    return as_bitarr

def readgroups(bitarr):
    ret = []
    bitsRead = 0
    for i in range(0, len(bitarr), 5):
        ret += bitarr[i+1:i+5]
        bitsRead += 5
        if not bitarr[i]:
            break

    return asdec(ret), bitsRead

def asdec(bitarr):
    ret = 0
    base = 1 << (len(bitarr) - 1)
    for val, bit in enumerate(bitarr):
        ret += bit * base
        base = base >> 1
    return ret

def asbitarr(code):
    bitarr = []
    shiftbit = 1
    while shiftbit < code:
        bitarr.insert(0, int(code & shiftbit > 0))
        shiftbit = shiftbit << 1

    return bitarr

def parse(bitarr, depth=0):
    tab = '  ' * depth
    version = asdec(bitarr[:3])
    ttype = asdec(bitarr[3:6])
    used_bits = 6
    
    # terminates
    if ttype == 4:
        dat, group_bits = readgroups(bitarr[6:])
        used_bits += group_bits
        # print(f'{tab}LITv{version} ({used_bits} b)')
        return used_bits, Package(ver=version, ttype=ttype, dat=dat)
    
    pckg = Package(ver=version, ttype=ttype, dat=None)
    # len type
    used_bits += 1
    if bitarr[6]:
        # 11 length bits
        used_bits += 11
        count = asdec(bitarr[7:18])
        # print(f'{tab}OPv{version} ({count}x11 b)')
        for i in range(count):
            parsed_bits, sub_pckg = parse(bitarr[used_bits:], depth+1)
            used_bits += parsed_bits
            pckg.add_sub(sub_pckg)
        return used_bits, pckg

    # 15 length bits
    used_bits += 15
    sub_bits = remaining_bits = asdec(bitarr[7:22])
    # print(f'{tab}OPv{version} ({sub_bits} b)')
    while remaining_bits:
        parsed_bits, sub_pckg = parse(bitarr[used_bits:], depth+1)
        remaining_bits -= parsed_bits
        used_bits += parsed_bits
        pckg.add_sub(sub_pckg)
    return used_bits, pckg

def versum(pckg):
    ret = pckg.ver
    for sp in pckg.subs:
        ret += versum(sp)
    return ret

def pprint(pckg, depth=0):
    ret = '  ' * depth + pckg._str() + '\n'
    for sp in pckg.subs:
        ret += pprint(sp, depth+1)
    return ret


transmissions = []
with open(sys.argv[1], 'r') as src:
    ln = src.readline()
    while ln:
        transmissions.append(ln.strip())
        ln = src.readline()

for i, ctr in enumerate(transmissions):
    print(ctr)
    _, pckg = parse(from_hexstring(ctr))
    print(f'---> {versum(pckg)}')
    print(pprint(pckg))
