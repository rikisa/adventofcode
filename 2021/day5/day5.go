package main


import (
    "fmt"
    // "errors"
    "math"
    "strconv"
    "regexp"
    // "strings"
    "aoc/inputs"
)

type Line struct {
    X0, Y0, X1, Y1 int
}

func (l Line) String() string {
    return fmt.Sprintf("(%3d,%3d) -> (%3d,%3d)", l.X0, l.Y0, l.X1, l.Y1)
}

// fails on verticals
func (l *Line) AsFunc() func(int) (int, bool) {
    var dx, dy, m, b, x0, x1, y0, y1 float64
    x0, y0, x1, y1 = float64(l.X0), float64(l.Y0), float64(l.X1), float64(l.Y1)

    dx = x1 - x0
    dy = y1 - y0
    m = dy / dx
    b = y0 - (m * x0)
    // fmt.Printf("%s: dx: %.1f, dy: %.1f, m: %.1f, b: %.1f\n", *l, dx, dy, m, b)
    return func(x_ int) (int, bool) {
        x := float64(x_)
        return int(math.Round(x * m + b)), ((x >= x0) && (x <= x1))
    }
}

func (l *Line) AsPlot() func(int, int) (int) {
    var m, b, dx, dy, x0, x1, y0, y1 float64
    x0, y0, x1, y1 = float64(l.X0), float64(l.Y0), float64(l.X1), float64(l.Y1)

    ymin := math.Min(y0, y1)
    ymax := math.Max(y0, y1)

    dx = x1 - x0
    dy = y1 - y0
    m = dy / dx
    b = y0 - (m * x0)

    return func(x_ int, y_ int) (int) {
        x , y := float64(x_), float64(y_)

        // check bounds
        inX := (x >= x0) && (x <= x1) // x is always ordered
        inY := (y >= ymin) && (y <= ymax)
        if !inX || !inY {
	    return 0
	}

        // plot
        wanty := math.Round(x * m + b)
        // fmt.Printf("(%.1f, %.1f) -> wanty: %.1f (m: %.1f b: %.1f) diff: %.3f\n", x, y, wanty, m, b, math.Abs(wanty - y))
        
        // axline
        if (x == x0) && (x == x1) && (ymin <= y) && (y <= ymax) {
            // fmt.Printf("1 (A)\n")
            return 1
        }

        if math.Abs(wanty - y) <= 1e-2 {
            // fmt.Printf("1 (w)\n")
            return 1
        }

        // fmt.Println(0)
        return 0
    }
}

func getLines() ([]Line, int, int) {

    var ret []Line
    var x0, y0, x1, y1 int
    var xdim, ydim int = -1, -1

    rePt := regexp.MustCompile(` *-> *`)
    reVals := regexp.MustCompile(` *, *`)
    

    scanner := inputs.AsLineScanner()
    for scanner.Scan() {
        var line []int
        lntxt := scanner.Text()
        for _, pt := range(rePt.Split(lntxt, -1)) {
            for _, pti := range(reVals.Split(pt, -1)) {
                value, err := strconv.Atoi(pti)
                if err != nil {panic(err)}
                line = append(line, value) 
            }
        }
        
        x0, y0, x1, y1 = line[0], line[1], line[2], line[3]
        if x0 > x1 {
            x0, y0, x1, y1 = x1, y1, x0, y0 
        } 
        ret = append(ret, Line{x0, y0, x1, y1})
        if x1 > xdim {
            xdim = x1
        }
        if y1 > ydim {
            ydim = y1
        }
        if y0 > ydim {
            ydim = y0
        }
    }

    return ret, xdim+1, ydim+1
}

// Solution to part one
func solution (keepDiags bool) {
    // for _, l := range(getLines()){
    //     fmt.Printf("L: %s\n", l)
    // }
    lines, xdim, ydim := getLines()
    canvas := make([][]int, ydim)
    for ri := range(canvas) {
        canvas[ri] = make([]int, xdim)
    }
    
    var lFuncs []func(int, int)(int)
    for _, l := range(lines) {
        if keepDiags {
            lFuncs = append(lFuncs, l.AsPlot())
        } else if (l.X0 == l.X1) || (l.Y0 == l.Y1) {
            lFuncs = append(lFuncs, l.AsPlot())
        }
    }

    // for x := 0; x < xdim; x++ {
    //     for _, fn := range(lFuncs) {
    //         y := fn(x)
    //         canvas[y][x] += 1
    //     }
    // }
    
    // fn := lFuncs[8]
    // fmt.Printf("%s\n", lines[8])
    for _, fn := range(lFuncs) {
        for x := 0; x < xdim; x++ {
            for y := 0; y < ydim; y++ {
                val := fn(x, y)
                // fmt.Printf("\tf(%d, %d) = %d\n", x, y, val)
                if val > 0 {
                    canvas[y][x] += 1
                }
            }
        }
    }
    
    var rowCnt, total int
    for _, crow := range(canvas) {
	    rowCnt = 0
    	for _, ccol := range(crow) {
            if ccol == 0 {
                fmt.Printf("%3s", ".")
            } else {
                fmt.Printf("%3d", ccol)
            }
            if ccol >= 2 {
                rowCnt += 1
            }
	    }
        total += rowCnt
        // fmt.Printf("%d > %d\n", crow, rowCnt)
        fmt.Printf(" > %d\n", rowCnt)
    }
    fmt.Printf("= %d\n", total)
}


func main () {
    if inputs.Which() == 1 {
        solution(false)
    } else {
        solution(true)
    }
    fmt.Printf("Done!\n")
}
