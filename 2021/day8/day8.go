package main


import (
    "fmt"
    "sort"
    "strconv"
    "regexp"
    "strings"
    // "errors"
    // "math"
    "aoc/inputs"
)

func sortPatterns(patterns []string) []string {

    ret := make([]string, len(patterns))
    
    for i, pat := range(patterns) {
        buf := []rune(pat)
        sort.Slice(buf, func(i int, j int) bool { return buf[i] < buf[j] })
        ret[i] = string(buf)
    }

    return ret
}

func getValues() ([][]string, [][]string) {
    reHS := regexp.MustCompile(` *\| *`)
    scanner := inputs.AsLineScanner()
    
    var signalPatterns [][]string
    var outputValues [][]string
    for scanner.Scan() {
        lntxt := scanner.Text()
        splits := reHS.Split(lntxt, 2)
        signalPatterns = append(signalPatterns,
            sortPatterns(strings.Fields(splits[0])))
        outputValues = append(outputValues,
            sortPatterns(strings.Fields(splits[1])))
    }

    return signalPatterns, outputValues
}


// Solution to part one
func solution() {

    _, outputValues := getValues()

    var knownLen = []int{
        2, // 1
        3, // 7
        4, // 4
        7, // 8
    }
    counts := make(map[int]int)
    var knownNums int

    
    for _, outputs := range(outputValues) {
        for _, seg := range(outputs) {
            counts[len(seg)] += 1
        }
    }

    for _, l := range(knownLen) {
        knownNums += counts[l]
    }

    
    fmt.Println(counts, knownNums)
}

type code struct {
    ln, on, fo, se int
}

var codeLUT map[code]int = map[code]int{ 
    //       same segments as... 
    //  len   v    v    v
    //   v    1    4    7 
    code{2,   2,   2,   2}: 1,
    code{5,   1,   2,   2}: 2,
    code{5,   2,   3,   3}: 3,
    code{4,   2,   4,   2}: 4,
    code{5,   1,   3,   2}: 5,
    code{6,   1,   3,   2}: 6,
    code{3,   2,   2,   3}: 7,
    code{7,   2,   4,   3}: 8,
    code{6,   2,   4,   3}: 9,
    code{6,   2,   3,   3}: 0,
}

func sameSegments(a string, b string) int {
    same := 0
    for _, ra := range(a) {
        for _, rb := range(b) {
            if ra == rb {
                same += 1
                break
            }
        }
    }
    return same
}

func makeMap(pats []string) map[string]int {
    
    // first pass, getting the known values
    refMap := make([]string, 4)
    for _, sp := range(pats) {
        switch {
            case len(sp) == 2:
                refMap[0] = sp
            case len(sp) == 4:
                refMap[1] = sp
            case len(sp) == 3:
                refMap[2] = sp
            case len(sp) == 7: // actually not needed
                refMap[3] = sp
        }
    }

    // calculate codes
    // fmt.Printf("RefMap %v", refMap)
    decoded := make(map[string]int)
    for _, sp := range(pats) {
        spCode := code{
            len(sp),
            sameSegments(sp, refMap[0]),
            sameSegments(sp, refMap[1]),
            sameSegments(sp, refMap[2]),
        }
        // fmt.Printf("%s -> %v -> %d\n\n", sp, spCode, codeLUT[spCode])
        decoded[sp] = codeLUT[spCode]
    }

    return decoded

}

// Solution to part two
func solution2() {

    signalPatterns, outputValues := getValues()
    longList := make([][]string, len(outputValues))
    for i := range(outputValues) {
        longList[i] = append(signalPatterns[i], outputValues[i]...)
    }

    // check if there are all known values present in data
    // hasValue := make(map[int]bool)
    // allTrue := true
    // for _, allVals := range(longList) {
    //     for _, sp := range(allVals) {
    //         hasValue[len(sp)] = true
    //     }
    //     hasAll := true
    //     for _, kn := range(knownLen) {
    //         hasAll = hasAll && hasValue[kn]
    //     }
    //     if !hasAll {allTrue = false}
    //     fmt.Printf("%s %v\n", allVals, hasAll)
    // }
    // fmt.Printf("-> %v\n", allTrue)

    // decode
    sum := 0
    for i, allVals := range(longList) {
        segMap := makeMap(allVals)
        joinedInt := ""
        for _, oV := range(outputValues[i]) {
            joinedInt = joinedInt + fmt.Sprintf("%d", segMap[oV])
        }
        val, err := strconv.Atoi(joinedInt) 
        if err != nil {panic(err)}
        fmt.Printf("%7s: %d\n", outputValues[i], val)
        sum += val
    }
    fmt.Printf("=> %d\n", sum)

}


func main () {
    if inputs.Which() == 1 {
        solution()
    } else {
        solution2()
    }
    fmt.Printf("Done!\n")
}
