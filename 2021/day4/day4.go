package main


import (
    "fmt"
    // "errors"
    // "math"
    "aoc/inputs"
)

// Solution to part one
func solution () {
    draws, bingos := inputs.AsBingos()
    // for _, b := range(bingos) {
    //     b.CheckNumber(7)
    //     b.CheckNumber(7)
    // }

    // fmt.Printf("%s\n", bingos[0])
    // for i, num := range(bingos[0].Numbers) {
    //     fmt.Printf("%d: %v\n", i, num)
    // }
    for di, dr := range(draws) {
        for bi, b := range(bingos) {
            b.CheckNumber(dr)
            if b.Wins {
                var unmarkedSum int
                for _, num := range(b.Numbers) {
                    if num.Checked == false {
                        unmarkedSum += num.Value
                    }
                }
                fmt.Printf("Board %d wins in after a %d in round %d (%d)\n", bi+1, dr, di, unmarkedSum*dr)
                return
            }
        }
    }
}

// Solution to part two
func solution2 () {
    draws, bingos := inputs.AsBingos()
    
    var winnerBingo, winnerNum, winnerCnt, winnerSums []int
    var skip bool
    for di, dr := range(draws) {
        for bi := 0; bi < len(bingos); bi++ {
            b := bingos[bi]
            b.CheckNumber(dr)
            
            // why IS WIN NOT PERSISTENT?!?
            // if b.Wins {continue} // cant win twice
            skip = false
            for _, wi := range(winnerBingo) {
                if wi == bi {
                    skip = true
                    break
                }
            }
            if skip {
                fmt.Printf("Skipping %d\n", bi)
                continue
            }

            if b.Wins {
                var unmarkedSum int
                for _, num := range(b.Numbers) {
                    if num.Checked == false {
                        unmarkedSum += num.Value
                    }
                }
                winnerBingo = append(winnerBingo, bi)
                winnerNum = append(winnerNum, dr)
                winnerCnt = append(winnerCnt, di)
                winnerSums = append(winnerSums, unmarkedSum * dr)
            }
        }
    }
    fmt.Printf("First board %d wins in after a %d in round %d (%d)\n",
        winnerBingo[0], winnerNum[0], winnerCnt[0]+1, winnerSums[0])
    j := len(winnerBingo) - 1
    fmt.Printf("Last board %d wins in after a %d in round %d (%d)\n",
        winnerBingo[j], winnerNum[j], winnerCnt[j]+1, winnerSums[j])

}


func main () {
    if inputs.Which() == 1 {
        solution()
    } else {
        solution2()
    }
    fmt.Printf("Done!\n")
}
