import sys

# general formular of motion
# constan tranlsational acceralation
# s = 0.5 * a * t**2 + v0*t + s0

# def calc_pos(a, v0, s0, n):
#     ret = ()
#     for i in range(2):
#         ri = 0.5 * a[i] * n**2 + v0[i] * n + s0[i]
#         ret += (ri,)
#     return ret
# 
# 
# tr = []
# for i in range(10):
#     tr.append(calc_pos(
#         [-1, -1],
#         [7, 2],
#         [0, 0],
#         n=i
#     ))
# 
# for tri in tr:
#     print(tri)

# if sys.argv[1] == '1':
# target = -10, -5, 20, 30
target = -118, -62, 235, 259

def in_target(pos):
    # overshot
    if pos[0] < target[0] or pos[1] > target[3]:
        return 1
    # hitting
    if target[1] >= pos[0] >= target[0] and \
       target[2] <= pos[1] <= target[3]:
           return 0
    # incoming
    return -1

def trace(p0, v0, a):
    curtr = []
    pos, vel = p0, v0
    while in_target(pos) <= 0:
        curtr.append(pos)
        pos = pos[0] + vel[0], pos[1] + vel[1]
        vel = vel[0] + a[0], max(vel[1] + a[1], 0)

    return curtr

def maxy(tr):
    best = -1
    for pt in tr:
        if pt[0] >= best:
            best = pt[0]
    return best

def show(v0):
    tr = trace((0, 0), v0, (-1, -1))
    print(tr[-1], in_target(tr[-1]) == 0, maxy(tr))

a = (-1, -1)
traces = {}
# could calculate direct pathes to find good values but meh
for vx in range(1, 400):
    for vy in range(-300, 300):
        v0 = (vy, vx)
        tr = trace((0, 0), v0, a)
        if in_target(tr[-1]) == 0:
            traces[v0] = tr

# show((9, 6))
best = (-1, -1), -1
for v0, tr in traces.items():
    maxytr = maxy(tr)
    if maxytr > best[1]:
        best = v0, maxytr

print(best, len(traces))
