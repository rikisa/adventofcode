import sys
import itertools as it

# get data
if __name__ == '__main__':
    # for timeit
    with open(sys.argv[1], 'r') as src:
        txt = src.readlines()
else:
    with open('../data/day14.txt', 'r') as src:
        txt = src.readlines()

# init polymer
poly = txt[0].strip()
# subsitition rules / templates
rules = {}
for t in txt[2:]:
    f, t = t.split(' -> ')
    rules[f] = t.strip()

def main():
    # all possible pairs
    def pairwise(s):
        a, b = it.tee(s, 2)
        next(b, None)
        return zip(a, b)

    pairs = {}
    for p in pairwise(poly):
        p = ''.join(p)
        pairs[p] = pairs.get(p, 0) + 1

    # make substitutions
    for s in range(40):
        new_pairs = {k: 0 for k in pairs.keys()}
        for p, cnt in pairs.items():
            sub = rules.get(p)
            if sub is None: continue
            subs = [p[0] + sub, sub + p[1]]
            for np in subs:
                new_pairs[np] = new_pairs.get(np, 0) + cnt
            # print(f'{p} ({cnt}) +{sub} >> {subs}')
        pairs = new_pairs

    # count
    elemcount = {}
    for k, v in pairs.items():
        elemcount[k[0]] = elemcount.get(k[0], 0) + v
    # last one will never be counted
    elemcount[poly[-1]] += 1

    # minmax and solution
    leastC = mostC = None
    least, most = float('inf'), 0
    for elm, cnt in elemcount.items():
        least = min(least, cnt)
        most = max(most, cnt)
        if least == cnt:
            leastC = elm
        elif most == cnt:
            mostC = elm
    return most, mostC, least, leastC

if __name__ == "__main__":

    # hahahahaha
    print('{most} {mostC} - {least} {leastC} = {diff}'
          .format(**dict((k, v) for k, v in zip(
            ('most', 'mostC', 'least', 'leastC', 'diff'),
            (lambda args:args+(args[0]-args[2],))(main())))
          )
    )
