package main


import (
    "fmt"
    // "errors"
    "math"
    "aoc/inputs"
)

// filter all bits that start with choose
func filterBitlist( bitlist []string, choose rune, atbit int) []string {
    var ret []string
    for _, bits := range(bitlist) {
        if rune(bits[atbit]) == choose {
            ret = append(ret, bits)
        }
    }
    return ret
}

// parse bits
func parseBits( bitcount []int, bits string) {
    for i, bit := range(bits) { 
        if bit == '1' {
            bitcount[i] += 1
        } else {
            bitcount[i] -= 1
        }
    }
}

// parse list of bits
func parseBitslist( bitslist []string ) []int {

    // buffer for bits
    var bitcount []int
    
    // expand bitcount buffer
    for i := 0; i < len(bitslist[0]); i++ {
        bitcount = append(bitcount, 0)
    }
    
    // process first line, grow bitbuff
    for _, bits := range(bitslist) { 
        parseBits( bitcount, bits )
    }
    
    return bitcount
}


// Solution to part one
func solution () {

    dat := inputs.AsStrings()
    bitcounts := parseBitslist(dat)
    
    var bitdepth float64 = float64(len(bitcounts))
    var gamma int = 0
    var epsilonmask int = int(math.Pow(2, bitdepth)) - 1

    fmt.Printf("%f bits\nGamma: '", bitdepth)
    for i, count := range(bitcounts) {
        if count > 0 {
            dec := math.Pow(2, bitdepth - float64(i) - 1)
            // fmt.Printf("1(%f, %d)", dec, int(dec))
            fmt.Printf("1")
            gamma += int(dec)
        } else {
            fmt.Printf("0")
        }
    }
    fmt.Printf("': %d\n", gamma)
    fmt.Printf("Epsilon: %d\n", epsilonmask ^ gamma)
    fmt.Printf("Power: %d\n", (epsilonmask ^ gamma) * gamma)

}

// recursively reduce bitlist
func reduceBitlist ( mode string, bitslist []string ) string {

    var filterA, filterB rune
    
    // switch reducing mode
    switch {
        case mode == "most":
            filterA = '1'
            filterB = '0'
        case mode == "least":
            filterA = '0'
            filterB = '1'
        default:
            fmt.Printf("???: '%s'",  mode)
            return "???"
    }

    bitcounts := parseBitslist(bitslist)
    var bitdepth int = len(bitcounts)

    // fmt.Printf("INIT: Bitcounts: %d | ",  bitcounts)
    // fmt.Printf("Bitslist: %s\n", bitslist)
    for i := 0; i < bitdepth; i++ {

        if bitcounts[i] >= 0 {
            bitslist = filterBitlist(bitslist, filterA, i)
        } else if bitcounts[i] < 0 {
            bitslist = filterBitlist(bitslist, filterB, i)
        }

        if len(bitslist) == 1 {
            break
        }

        bitcounts = parseBitslist(bitslist)

        // fmt.Printf("%d: Bitcounts: %d | ", i, bitcounts)
        // fmt.Printf("Bitslist: %s\n", bitslist)

    }

    return bitslist[0]

}

// Solution to part two
func solution2 () {

    bitslist := inputs.AsStrings()

    oxy := reduceBitlist("most", bitslist)
    scr := reduceBitlist("least", bitslist)

    fmt.Printf("OxyGen (most): %s (%d)\n", oxy, inputs.ToDec(oxy))
    fmt.Printf("XScrub (least): %s (%d)\n", scr, inputs.ToDec(scr))
    fmt.Printf("Prod: %d\n", inputs.ToDec(scr) * inputs.ToDec(oxy))

}


func main () {
    if inputs.Which() == 1 {
        solution()
    } else {
        solution2()
    }
    fmt.Printf("Done!\n")
}
