package main


import (
    "fmt"
    // "errors"
    // "math"
    // "strconv"
    // "regexp"
    // "strings"
    "aoc/inputs"
)

// Solution to part one
func solution (maxSteps int) {
    initStates := inputs.AsCSV()

    var stateNum = [9]int64{0, 1, 2, 3, 4, 5, 6, 7, 8}
    var curState = [9]int64{0, 0, 0, 0, 0, 0, 0, 0, 0}
    var nxtState = [9]int64{0, 0, 0, 0, 0, 0, 0, 0, 0}
    var fishies int64

    for _, s0 := range(initStates) {
        curState[s0] += 1
    }

    fmt.Printf("%3s %2d\n", "S", stateNum)
    fmt.Printf("%3d %2d\n", 0, curState)

    for step := 1; step <= maxSteps; step++ {
        
        // age them all by moving the state counts
        fishies = 0
        for i := 8; i > 0; i--{
            nxtState[i-1] = curState[i]
            fishies += curState[i]
        }
        
        // 0 state becomes 6 state
        nxtState[6] += curState[0]

        // 0 and children
        nxtState[8] = curState[0]

        fishies += 2 * curState[0]
        
        // update
        curState = nxtState

        fmt.Printf("%3d %2d = %d\n", step, curState, fishies)
    }
}

// Solution to part two
func solution2 () {
}


func main () {
    if inputs.Which() == 1 {
        solution(80)
    } else {
        solution(256)
    }
    fmt.Printf("Done!\n")
}
