module aoc/day15

go 1.17

replace aoc/inputs => ../inputs

replace fatih/color => /home/rikisa/go/pkg/mod/github.com/fatih/color@v1.13.0/

replace mattn/go-colorable => /home/rikisa/go/pkg/mod/github.com/mattn/go-colorable

replace mattn/go-isatty => /home/rikisa/go/pkg/mod/github.com/mattn/go-isatty

replace x/sys => /home/rikisa/go/pkg/mod/golang.org/x/sys

require (
	aoc/inputs v0.0.0-00010101000000-000000000000
	fatih/color v0.0.0-00010101000000-000000000000
	rthornton128/goncurses v0.0.0-00010101000000-000000000000
)

require (
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/rthornton128/goncurses v0.0.0-20211122162138-db8d4cdb33a9 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect

)

replace rthornton128/goncurses => /home/rikisa/go/pkg/mod/github.com/rthornton128/goncurses@v0.0.0-20211122162138-db8d4cdb33a9
