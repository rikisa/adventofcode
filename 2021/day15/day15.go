package main


import (
    "fmt"
    // "time"
    // "bytes"
    // "math/rand"
    // "sync"
    // "sort"
    // "strconv"
    // "regexp"
    // "strings"
    // "errors"
    // "reflect"
    "aoc/inputs"
    // "fatih/color"
    // "rthornton128/goncurses"
)

func getLut() map[int]string{
    return map[int]string{
        -1: (" "),
         10: ("\033[38;2;25;0;0m▁\033[0m"),
         50: ("\033[38;2;80;0;0m▃\033[0m"),
         100: ("\033[38;2;110;25;0m▄\033[0m"), 
         150: ("\033[38;2;140;50;0m▅\033[0m"), 
         200: ("\033[38;2;175;100;0m▆\033[0m"),
         250: ("\033[38;2;255;230;100m▇\033[0m"),
      -999: ("?"),
     }
}

var CostLUT map[int]string = getLut()

type Point struct {
    Y, X, Val int
}

func isVisited(n Point, arr []Point) bool {
    for _, dst := range arr {
        if n.Y == dst.Y && n.X == dst.X {return true}
    }
    return false
}

func PlotTrace(cv [][]int, trace []Point) string {
    color := make([][]int, len(cv))
    var traceVal int
    for i, row := range(cv) {
        color[i] = make([]int, len(row))
    }
    for _, pt := range(trace) {
        color[pt.Y][pt.X] += 1
        traceVal += pt.Val
    }
    ret := ""
    var pchar string
    for i, row := range cv {
        for j := range row {
            // choose char to print
            if cv[i][j] < 0 {
                switch {
                    case i == 0 && j == 0:
                        pchar = " "
                    case i == 0:
                        if j % 2 == 0 {
                            pchar = fmt.Sprintf("\033[38;2;120;80;0m%d\033[0m", j % 10)
                        } else {
                            pchar = fmt.Sprintf("\033[38;2;80;120;0m%d\033[0m", j % 10)
                        }
                    case j == 0:
                        if i % 2 == 0 {
                            pchar = fmt.Sprintf("\033[38;2;120;80;0m%d\033[0m", i % 10)
                        } else {
                            pchar = fmt.Sprintf("\033[38;2;80;120;0m%d\033[0m", i % 10)
                        }
                    default:
                        pchar = "x"
            }
            } else {
                pchar = fmt.Sprintf("%d", cv[i][j])
            }
            // color to use
            if color[i][j] == 1 {
                pchar = fmt.Sprintf("\033[38;2;255;230;100m%s\033[0m", pchar)
            } else if color[i][j] > 1 {
                pchar = fmt.Sprintf("\033[38;2;255;40;80m%s\033[0m", pchar)
            }
            ret += pchar
        }
        ret += "\n"
    }
    ret += fmt.Sprintf("=> %d\n", traceVal)
    return ret
}

var costCache map[Point]int = map[Point]int{}
var traceCache map[Point][]Point = map[Point][]Point{}
var cacheHits int
var worstVal int
func getCached(pt Point) ([]Point, int, bool) {
    val, ok := costCache[pt]
    if !ok {
        return []Point{}, -1, false
    }
    cacheHits += 1
    return traceCache[pt], val, true
}

func cacheRet(start Point, retTrace []Point, retVal int) {
    bestRet, ok := costCache[start]
    if !ok {
        costCache[start] = retVal
        traceCache[start] = retTrace
    } else {
        if retVal < bestRet {
            costCache[start] = retVal
            traceCache[start] = retTrace
        }
    }
}

// remove all the checking
// for horizontal and vertical neighbours
// var hvidx [][]int = [][]int{[]int{0, 1}, []int{1, 0}, []int{0, -1}, []int{-1, 0}}
// no going back
var calls, reached, stopped int
var hvidx [][]int = [][]int{[]int{1, 0}, []int{0, 1}}
// trace val follows from top to bottom to skipp subprime traces
func walk(cv [][]int, start, stop Point, curVal int) ([]Point, int) {

    calls += 1
    fmt.Printf("\rC: %d, R: %d, K: %d, C: %d", calls, reached, stopped, cacheHits)
    
    // stop at end
    if start == stop {
        reached += 1
        return []Point{}, 0
    }

    // get candidate neighbours
    var cands []Point

    for _, idx := range hvidx {
        ri, cj := idx[0] + start.Y, idx[1] + start.X
        // skipp padding
        if cv[ri][cj] == -1 {continue}
        cands = append(cands, Point{Y: ri, X: cj, Val: cv[ri][cj]})
    }
    
    var bestTrace []Point
    // init to worst possible value
    bestVal := worstVal
    for _, cand := range cands {
        subTrace, subVal, ok := getCached(cand)
        if !ok {
            subTrace, subVal = walk(cv, cand, stop, curVal + cand.Val)
            traceCache[cand] = subTrace
            costCache[cand] = subVal
        }
        cval := cand.Val
        // if inc {
        //     tileR int = cand.Y / len(cv)
        //     tileC int = cand.X / len(cv[cand.Y])
        //     cval += tileR + tileC
        // }
        if subVal + cval < bestVal {
            bestVal = subVal + cval
            bestTrace = append([]Point{cand}, subTrace...)
        }
    }

    return bestTrace, bestVal
}


// Solution to part one
func solution(cavern [][]int ) {
    worstVal = len(cavern) * len(cavern) * 10
    start := Point{1, 1, cavern[1][1]}
    stoprow := len(cavern)-2
    stopcol := len(cavern[stoprow])-2
    stop := Point{stoprow, stopcol, cavern[stoprow][stopcol]}
    _, v := walk(cavern, start, stop, 0)
    fmt.Println("")
    // fmt.Println(PlotTrace(cavern, tr))
    // fmt.Println(tr, v)
    fmt.Println(v)
    ret := 0
    fmt.Println(ret)
}


// Solution to part two
func solution2(cavern [][]int) {
    tiles := 5
    extra := make([][]int, len(cavern)*tiles)
    for r := range cavern {
        for i := 0; i<tiles; i++ {
            offr := len(cavern) * i
            extra[r+offr] = make([]int, len(cavern[r])*tiles)
        }
    }

    
    for r, row := range cavern {
        for c, val := range row {
            for i := 0; i<tiles; i++ {
                offr := len(cavern) * i
                for j := 0; j<tiles; j++ {
                    offc := len(row) * j
                    // fmt.Println(offr, offc)
                    newVal := ((val-1) + i + j) % 9 + 1
                    if newVal == 0 {newVal++}
                    extra[r+offr][c+offc] = newVal
                }
            }
        }
    }

    // fmt.Printf("%d x %d", len(extra), len(extra[0]))
    solution(inputs.PadArr(extra, -1))
}

func fillcostmap(cavern [][]int, start Point, stop Point, costmap [][]int) {

    // set stop value
    costmap[stop.Y][stop.X] = stop.Val
    
    // from stop onwards
    for r, row := range cavern {
        r = stop.Y - r
        for c := range row {
            c = stop.X - c
            for _, idx := range hvidx {
                ri, cj := r - idx[0], c - idx[1]
                // skip boarders
                if cavern[ri][ci] == -1 {continue}
                curCost = cavern[ri][ci] + cavern[r][c]
                if costmap[ri][ci] > curCost {
                    costmap[ri][ci] = curCost
                }
            }
        }
    }
    return costmap
}


func solution2m(cavern [][]int) {
    worstVal = len(cavern) * len(cavern) * 10
    
    // make the costmap
    costmap := make([][]int, len(cavern))
    for i, row := range cavern {
        costmap[i] = make([]int, len(row))
        for j, v := range(row) {
            if v != -1 {
                costmap[i][j] = worstVal
            } else {
                costmap[i][j] = -1
            }
        }
    }



    start := Point{1, 1, cavern[1][1]}
    stoprow := len(cavern)-2
    stopcol := len(cavern[stoprow])-2
    stop := Point{stoprow, stopcol, cavern[stoprow][stopcol]}

    cmap := fillcostmap(cavern, start, stop, costmap)
}


func main () {
    // if inputs.WhichSecond() == 1 {
    //     solution(inputs.AsPaddedDenseDigitsOnly(-1))
    // } else {
    //     solution2(inputs.AsDenseDigitsOnly())
    // }
    solution2m(inputs.AsPaddedDenseDigitsOnly(-1))
    fmt.Printf("Done!\n")
}
