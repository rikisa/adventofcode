use regex::Regex;
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::process;

#[cfg(test)]
mod tests {
    use super::*;

    fn get_testcodes() -> () {}

    #[test]
    fn day03_example_1() {}

    #[test]
    fn day03_example_2() {}
}

fn read_values(mem: &mut String) -> Result<(), Box<dyn Error>> {
    let file_path;
    if let Some(path) = env::args_os().nth(1) {
        file_path = path;
    } else {
        return Err(From::from("No path to text provided!"));
    };

    let file = File::open(file_path)?;
    let reader = BufReader::new(file);

    // Iterate over each line in the file
    for line in reader.lines() {
        let line = line?; // Unwrap the Result to get the line as a String
        mem.push_str(&line);
    }

    Ok(())
}

fn parseit(mem: &str) {
    let do_token = "do()";
    let dont_token = "don't()";
    let mul_token = "mul(";

    let mut aggregate = true;
    let mut mul_sum = 0;

    for (start_idx, _) in mem.chars().enumerate() {
        if start_idx + dont_token.len() >= mem.len() {
            break;
        }

        if &mem[start_idx..start_idx + do_token.len()] == do_token {
            println!("Found do()");
            aggregate = true;
            continue;
        }

        if &mem[start_idx..start_idx + dont_token.len()] == dont_token {
            println!("Found don't()");
            aggregate = false;
            continue;
        }

        if &mem[start_idx..start_idx + mul_token.len()] == mul_token {
            println!("Found mul()");

            let mut mul_idx = start_idx + mul_token.len();

            let mut value0 = String::new();
            let mut value1 = String::new();

            let mut mul_char = &mem[mul_idx..mul_idx + 1];
            while "0123456789".contains(mul_char) {
                value0.push_str(mul_char);
                mul_idx += 1;
                mul_char = &mem[mul_idx..mul_idx + 1];
            }
            if mul_char != "," {
                continue;
            }

            mul_idx += 1;
            let mut mul_char = &mem[mul_idx..mul_idx + 1];
            while "0123456789".contains(mul_char) {
                value1.push_str(mul_char);
                mul_idx += 1;
                mul_char = &mem[mul_idx..mul_idx + 1];
            }
            if mul_char != ")" {
                continue;
            }

            let mut mul = 1;
            if let Ok(ret0) = value0.parse::<i32>() {
                mul *= ret0;
            } else {
                continue;
            }

            if let Ok(ret1) = value1.parse::<i32>() {
                mul *= ret1;
            } else {
                continue;
            }

            if aggregate {
                println!("\t{}, {} -> {}", value0, value1, mul);
                mul_sum += mul
            } else {
                println!("\t{}, {} -> ignored", value0, value1);
            }

        }
    }
    println!("SUM: {}", mul_sum);

}

fn main() {
    let mut hay = String::new();

    if let Err(err) = read_values(&mut hay) {
        println!("{}", err);
        process::exit(1);
    };
    // let hay = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))";
    // let hay2 = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";

    parseit(&hay);

    // let re = Regex::new(r"mul\([0-9]*,[0-9]*\)").unwrap();

    // let mut mul_sum = 0;
    // for ma in re.find_iter(&hay) {
    //     let m = ma.as_str();
    //     let mut val = 1;
    //     for part in m.replace("mul(", "").replace(")", "").split(',') {
    //         let cur_val = part.parse::<i32>().expect("Not a value, regex failed!");
    //         val *= cur_val;
    //     }
    //     mul_sum += val;

    //     println!("{} = {}", m, val);
    // }
    // println!("SUM: {}", mul_sum);
}
