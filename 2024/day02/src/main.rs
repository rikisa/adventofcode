use std::env;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::process;

#[derive(PartialEq)]
enum CodeDir {
    INC,
    DEC,
}

fn calc_line_safeness(line: &Vec<i32>, damp: i32) -> bool {
    if line.len() < 2 {
        return false;
    }

    let mut errors = 0;

    let mut cur_idx: usize = 0;
    let mut nxt_idx: usize = 1;

    let code_dir = if line[0] > line[1] {
        CodeDir::DEC
    } else {
        CodeDir::INC
    };

    // for (index, val) in line.iter().enumerate() {
    while cur_idx < line.len() && nxt_idx < line.len() {
        let cur_val = line[cur_idx];
        let nxt_val = line[nxt_idx];

        // Define dir
        let this_dir = if cur_val > nxt_val {
            CodeDir::DEC
        } else {
            CodeDir::INC
        };
        let diff_dir = this_dir != code_dir;

        // define diff
        let diff = (nxt_val - cur_val).abs();
        let out_range = diff < 1 || diff > 3;

        if out_range || diff_dir {
            errors += 1;
            nxt_idx += 1;
            continue;
        }

        // normal increment
        cur_idx = nxt_idx;
        nxt_idx = cur_idx + 1;
    }

    println!(
        "{} -> {} -> {}",
        line.iter()
            .map(|n| n.to_string())
            .collect::<Vec<_>>()
            .join(", "),
        errors,
        errors <= damp
    );

    errors <= damp
}

fn calc_safeness(codes: &Vec<Vec<i32>>, damp: i32) -> Vec<bool> {
    let mut safeness = Vec::new();

    for line in codes.iter() {
        let is_safe = calc_line_safeness(line, damp);
        // println!(
        //     "{} -> {}",
        //     line.iter()
        //         .map(|n| n.to_string())
        //         .collect::<Vec<_>>()
        //         .join(", "),
        //     is_safe
        // );
        safeness.push(is_safe);
    }

    safeness
}

fn safe_count(safeness: &Vec<bool>) -> i32 {
    let mut safe_count = 0;
    for is_safe in safeness.iter() {
        if *is_safe {
            safe_count += 1;
        }
    }
    safe_count
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_testcodes() -> Vec<Vec<i32>> {
        let ret = vec![
            vec![7, 6, 4, 2, 1],
            vec![1, 2, 7, 8, 9],
            vec![9, 7, 6, 2, 1],
            vec![1, 3, 2, 4, 5],
            vec![8, 6, 4, 4, 1],
            vec![1, 3, 6, 7, 9],
        ];
        ret
    }

    #[test]
    fn day02_example_1() {
        let mut codes = get_testcodes();
        let safeness = calc_safeness(&mut codes, 0);

        assert_eq!(safe_count(&safeness), 2);
    }

    #[test]
    fn day02_example_2() {
        let mut codes = get_testcodes();
        let safeness = calc_safeness(&mut codes, 1);

        assert_eq!(safe_count(&safeness), 4);
    }
}

fn read_values(codes: &mut Vec<Vec<i32>>) -> Result<(), Box<dyn Error>> {
    let file_path;
    if let Some(path) = env::args_os().nth(1) {
        file_path = path;
    } else {
        return Err(From::from("No path to csv provided!"));
    };

    let file = File::open(file_path)?;
    let reader = BufReader::new(file);

    // Iterate over each line in the file
    for line in reader.lines() {
        let line = line?; // Unwrap the Result to get the line as a String
        let stripped_line = line.trim(); // Strip control sequences (leading/trailing whitespace)

        let mut new_codes: Vec<i32> = Vec::new();

        // Split the line and parse each part as an integer
        for part in stripped_line.split(';') {
            let cur_val = part.parse::<i32>()?;
            new_codes.push(cur_val);
        }
        codes.push(new_codes);
    }

    Ok(())
}

fn main() {
    let mut codes = Vec::new();

    if let Err(err) = read_values(&mut codes) {
        println!("{}", err);
        process::exit(1);
    };

    let safeness = calc_safeness(&codes, 1);
    println!("Safe lines: {}", safe_count(&safeness));
}
