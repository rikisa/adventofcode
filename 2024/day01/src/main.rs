use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::fs::File;
use std::process;

fn calc_distance(left: &mut Vec<i32>, right: &mut Vec<i32>) -> i32 {
    let mut diff_val: i32 = 0;
    left.sort();
    right.sort();
    for (index, left_value) in left.iter().enumerate() {
        let right_value = match right.get(index) {
            Some(value) => value,
            None => {
                println!("List have different sizes!");
                // returning left value -> diff is zero
                left_value
            }
        };
        diff_val += (left_value - right_value).abs();
    }
    diff_val
}

fn calc_similarity(left: &mut Vec<i32>, right: &mut Vec<i32>) -> i32 {
    let mut right_counts = HashMap::new();

    for right_val in right.iter() {
        right_counts
            .entry(right_val)
            .and_modify(|counter| *counter += 1)
            .or_insert(1);
    }

    let mut similarity = 0;
    for left_val in left.iter() {
        let val_count = match right_counts.get(left_val) {
            Some(count) => count,
            None => continue,
        };
        similarity += val_count * left_val;
    }

    similarity
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day01_example_1() {
        let mut left: Vec<i32> = vec![3, 4, 2, 1, 3, 3];
        let mut right: Vec<i32> = vec![4, 3, 5, 3, 9, 3];

        let total_dist = calc_distance(&mut left, &mut right);
        assert_eq!(total_dist, 11);
    }

    #[test]
    fn day01_example_2() {
        let mut left: Vec<i32> = vec![3, 4, 2, 1, 3, 3];
        let mut right: Vec<i32> = vec![4, 3, 5, 3, 9, 3];

        let total_sim = calc_similarity(&mut left, &mut right);
        assert_eq!(total_sim, 31);
    }
}

type Lists = (i32, i32);

fn read_values(left: &mut Vec<i32>, right: &mut Vec<i32>) -> Result<(), Box<dyn Error>> {
    let file_path;
    if let Some(path) = env::args_os().nth(1) {
        file_path = path;
    } else {
        return Err(From::from("No path to csv provided!"));
    };

    let file = File::open(file_path)?;

    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(b';')
        .from_reader(file);

    for result in rdr.deserialize() {
        let record: Lists = result?;
        // println!("{} <> {}", record.0, record.1);
        left.push(record.0);
        right.push(record.1);
    }
    Ok(())
}

fn main() {
    let mut left = Vec::new();
    let mut right = Vec::new();

    if let Err(err) = read_values(&mut left, &mut right) {
        println!("{}", err);
        process::exit(1);
    }
    let dist = calc_distance(&mut left, &mut right);
    println!("Total distance is {}", dist);

    let sim = calc_similarity(&mut left, &mut right);
    println!("Similarity score is {}", sim);
}
